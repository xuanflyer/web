from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
import json, os
from mysite import settings


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

#return HttpResponse("Hello, world. welcome to the page of %s" % request.path)
def getinfo(request):
	output = "host = %s" % request.get_host()
	output += "<br>full_path = %s" % request.get_full_path() 
	output += "<br>user_agent = " + request.META.get('HTTP_USER_AGENT', 'unknown')
	output += "<br>your own ip = " + request.META.get('REMOTE_ADDR', 'unknown')
	output += "<br>referrer = " + request.META.get('HTTP_REFERER', 'unknown')
	output += "<br>request method = " + request.META.get('REQUEST_METHOD', 'unknown')
	return HttpResponse(output)

#return HttpResponse(output)
#return render(request, "static/parameters.html", locals())
def getparameters(request):
	output = request.GET
	return render(request, "static/parameters.html", locals())

def getjson(request):
	response_data = {}  
	response_data['result'] = 'failed'  
	response_data['message'] = 'You messed up'  
	response_data['name'] = 'xuanxuan'  
	response_data['sex'] = 'male'  
	response_data['wife'] = 'zhenzhen'  
	return HttpResponse(json.dumps(response_data), content_type="application/json") 

def getjson1(request):
	response_data = []
	response_data.append({'name': 'wyx'})
	response_data.append({'name': 'wyx'})
	response_data.append({'name': 'wyx'})
	response_data.append({'name': 'wyx'})
	return HttpResponse(json.dumps(response_data), content_type="application/json") 

def gettxt(request):
	MAX_SIZE = 1000
	fp = open(settings.BASE_DIR + "/wyx/templates/static/time.json", "r")
	fp.seek(0, os.SEEK_END)
	file_len = fp.tell()

	output = "hello"
	if file_len > MAX_SIZE:
		fp.seek(-MAX_SIZE, os.SEEK_CUR)
		lines = fp.readlines()
		if len(lines) > 1:
			output = "<br>".join(lines[1:])
	else:
		fp.seek(0, os.SEEK_SET)
		lines = fp.readlines()
		output = "<br>".join(lines[0:])
	return HttpResponse(output)




