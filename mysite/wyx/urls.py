from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^getinfo/$', views.getinfo, name='getinfo'),
    url(r'^getparameters/$', views.getparameters, name='getparameters'),
    url(r'^getjson/$', views.getjson, name='getjson'),
    url(r'^getjson1/$', views.getjson1, name='getjson1'),
    url(r'^gettxt/$', views.gettxt, name='gettxt'),
]
